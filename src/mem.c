#define _DEFAULT_SOURCE

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define ALIGN(n, a) (((n) % (a)) ? (n) - (n) % (a) + (a) : (n))

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}
static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz,
                       void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
  size_t size =
      region_actual_size(query + offsetof(struct block_header, contents));
  void *new_pages = map_pages(addr, size, MAP_FIXED_NOREPLACE);
  if (new_pages == MAP_FAILED) {
    new_pages = map_pages(addr, size, 0);
    if (new_pages == MAP_FAILED) {
      return REGION_INVALID;
    }
  }

  block_init(new_pages, (block_size){size}, NULL);

  return (struct region){
      .addr = new_pages,
      .size = size,
      .extends = (addr == new_pages),
  };
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region)) {
    /* err("heap_init(): unable to allocate starting region"); */
    return NULL;
  }

  return region.addr;
}

#define BLOCK_MIN_CAPACITY (alignof(max_align_t) * 2)

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  if (!block_splittable(block, query)) {
    return false;
  }

  size_t old_cap = block->capacity.bytes;
  block->capacity.bytes = query;
  struct block_header *new_block = block_after(block);
  block_init(new_block, (block_size){old_cap - query}, block->next);
  block->next = new_block;

  return true;
}

/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
  if (block->next == NULL || !mergeable(block, block->next)) {
    return false;
  }

  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;

  // perform a trick to prevent heap pointer leaks
  struct block_header *old_next = block->next;
  block->next = block->next->next;
  old_next->next = NULL;

  return true;
}

static inline void collapse_block(struct block_header *block) {
  while (try_merge_with_next(block)) {
  }
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static struct block_search_result
find_good_or_last(struct block_header *restrict block, size_t sz) {
  struct block_header *last = block;
  for (struct block_header *cur = block; cur != NULL;
       last = cur, cur = cur->next) {
    collapse_block(cur);

    if (cur->is_free && block_is_big_enough(sz, cur)) {
      return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, cur};
    }
  }

  return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, last};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }

  return result;
}

static struct block_header *grow_heap(struct block_header *restrict last,
                                      size_t query) {
  struct region new_region = alloc_region(block_after(last), query);
  if (region_is_invalid(&new_region)) {
    /* err("grow_heap(): unable to allocate new region"); */
    return NULL;
  }
  struct block_header *new_block = new_region.addr;
  last->next = new_block;

  if (try_merge_with_next(last)) {
    return last;
  } else {
    return new_block;
  }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  query = size_max(ALIGN(query, alignof(max_align_t)), BLOCK_MIN_CAPACITY);

  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if (result.type == BSR_REACHED_END_NOT_FOUND) {
    grow_heap(result.block, query);
    result = try_memalloc_existing(query, heap_start);
  }

  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    return result.block;
  } else {
    return NULL;
  }
}

static struct block_header *heap_page_term(struct block_header *start) {
  struct block_header *cur = start;
  for (; blocks_continuous(cur, cur->next); cur = cur->next) {
  }
  struct block_header *next_page = cur->next;

  if (munmap(start, (void *)cur - (void *)start + cur->capacity.bytes) == -1) {
    err("heap_page_term(): munmap(%p, ...) failed", start);
  }

  return next_page;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
  for (struct block_header *block = HEAP_START;
       (block = heap_page_term(block)) != NULL;) {
  }
}

void *_malloc(size_t query) {
  struct block_header *const addr =
      memalloc(query, (struct block_header *)HEAP_START);
  if (addr != NULL) {
    return addr->contents;
  } else {
    return NULL;
  }
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}

void _free(void *mem) {
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  collapse_block(header);
}
