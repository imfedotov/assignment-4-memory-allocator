#include <assert.h>
#include <stdalign.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"

#define ALMOST_PAGE 4000
#define PAGE 4096
#define SLACK 32 // space for header

#define TEST(testing_expr)                                                     \
  do {                                                                         \
    heap_init(PAGE * 2 - SLACK);                                               \
    testing_expr;                                                              \
    heap_term();                                                               \
  } while (0)

#define PTR_ALIGNED(p) ((uintptr_t)(p) % alignof(max_align_t) == 0)

void test_normal() {
  void *x = _malloc(1337);
  assert(x != NULL && PTR_ALIGNED(x));
}

void test_free_one() {
  void *x = _malloc(1337), *y = _malloc(228), *z = _malloc(420);
  assert(x != NULL && y != NULL && z != NULL);
  assert(PTR_ALIGNED(x) && PTR_ALIGNED(y) && PTR_ALIGNED(z));

  _free(y);

  void *w = _malloc(128);
  assert(w != NULL && PTR_ALIGNED(w));
  assert(x < w && w < z);
}

void test_free_two() {
  void *x = _malloc(1337), *y = _malloc(228), *z = _malloc(420);
  assert(x != NULL && y != NULL && z != NULL);
  assert(PTR_ALIGNED(x) && PTR_ALIGNED(y) && PTR_ALIGNED(z));

  _free(y);
  _free(x);

  void *w = _malloc(1515);
  assert(w != NULL && PTR_ALIGNED(w));
  assert(x == w && w < z);
}

void test_new_region_extends() {
  // takes up almost two heap pages (of 2 initialized)
  void *x = _malloc(ALMOST_PAGE * 2);
  assert(x != NULL && PTR_ALIGNED(x));
  void *y = _malloc(ALMOST_PAGE);
  assert(y != NULL && PTR_ALIGNED(y));

  // y starts on the second page and spans through to the third
  assert(y - x < PAGE * 2);
}

void test_new_region_someplace() {
  void *x = _malloc(ALMOST_PAGE * 2);
  assert(x != NULL && PTR_ALIGNED(x));
  uintptr_t xp = (uintptr_t)x;

  // mmap() adjacent page to force MAP_FIXED fail in alloc_region
  uintptr_t next_page = xp - xp % PAGE + PAGE * 2;
  void *actual_page = mmap((void *)0 + next_page, 1, PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  assert(next_page == (uintptr_t)actual_page);

  void *y = _malloc(ALMOST_PAGE); // won't fit into last block, new will be allocated
  assert(y != NULL && PTR_ALIGNED(y));
  // y's start ends up on a much higher page
  assert(y - x > PAGE * 2);
}

int main() {
  TEST(test_normal());
  TEST(test_free_one());
  TEST(test_free_two());
  TEST(test_new_region_extends());
  TEST(test_new_region_someplace());
}
